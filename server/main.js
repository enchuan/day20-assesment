//Load dependencies
var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var Sequelize = require("sequelize");


app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

/* Serve files from public directory  __dirname is the absolute path of
 the application directory
 */

app.use(express.static(__dirname + "/../client/"));

//TODO :Create a connection with mysql DB <HERE>
var MYSQL_USERNAME = 'root';
var MYSQL_PASSWORD = 'mysqlpw';
var conn = new Sequelize('grocery_list', MYSQL_USERNAME, MYSQL_PASSWORD, {});

var Brand = require('./models/brand')(conn, Sequelize);
var name = require('./models/name')(conn, Sequelize);
var upc12 = require('./models/upc12')(conn, Sequelize);



app.get("/api/grocery_list", function (req, res) {
    console.log("-- GET /api/name");
    Brand
        .findAll({

            limit: 20             // limit to 20
            , attributes: ['id', 'upc12', 'brand', 'name']
        })

                .then(function (name) {
                    console.log("-- GET /api/name Name then() \n " + JSON.stringify(name));
                    res.json(name);
                })
                .catch(function (err) {
                    console.log("-- GET /api/name Name catch() \n " + JSON.stringify(err));
                    console.log("err " + err);
                    res
                        .status(500)
                        .json({error: true})
                });

});
//Start the web server on port 3000
    app.listen(3000, function () {
        console.info("Webserver started on port 3000");
    });
