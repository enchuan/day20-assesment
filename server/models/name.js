module.exports = function(conn, Sequelize) {
    var name =  conn.define('name', {
        name: {
            type: Sequelize.STRING,
            allowNull: false
        }
    }, {
        tableName: 'grocery_list',
        timestamps: false
    });
    return name;
};