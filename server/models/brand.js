//Create a model for brandname table
module.exports = function (conn, Sequelize) {
    var Brand = conn.define("brandname", {
        brand: {
            type: Sequelize.STRING

        }
    }, {
        tableName: 'grocery_list',
        timestamps: false
    });

    return Brand;
};