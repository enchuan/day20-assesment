//Create a model for upc12 table
module.exports = function (conn, Sequelize) {
    var upc12 = conn.define("upc12", {
        upc12: {
            type: Sequelize.INTEGER(12)

        }
    }, {
        tableName: 'grocery_list',
        timestamps: false
    });

    return upc12;
};