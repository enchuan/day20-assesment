(function () {
    angular
        .module("GroceryApp")
        .config(GroceryAppConfig);
    GroceryAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function GroceryAppConfig($stateProvider,$urlRouterProvider){

        $stateProvider
            .state('A',{
                url : '/A',
                templateUrl :'./search/search.html',
                controller : 'SearchCtrl',
                controllerAs : 'ctrl'
            })
            .state('B',{
                url : '/B',
                templateUrl :'./edit/edit.html',
                controller : 'EditCtrl',
                controllerAs : 'ctrl'
            })
            .state('B1',{
                url : '/B1/:upc12',
                templateUrl :'./edit/edit.html',
                controller : 'EditCtrl',
                controllerAs : 'ctrl'
            });

        $urlRouterProvider.otherwise("/A");


    }
})();