(function () {
    'use strict';
    angular
        .module("GroceryApp")
        .controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = ["$http", "$filter","$stateParams","EditService"];
    function EditCtrl($http, $filter,$stateParams,EditService) {
        console.log("-- in edit.controller.js");
        // Exposed variables
        var vm = this;
//