(function () {
    angular
        .module("GroceryApp")
        .controller("SearchCtrl", SearchCtrl);

    SearchCtrl.$inject = ["$http", "$state", "SearchService"];

    function SearchCtrl($http, $state, SearchService) {
        var vm = this;

        vm.productName = '';
        vm.result = null;
        vm.showBrand = false;

        // Exposed functions
        vm.search = search;
        vm.searchForBrand = searchForBrand;

        // Initial calls
        init();


        // Function definitions
        function init() {
            SearchService.initProductData()
                .then(function (response) {
                    vm.brand = response;
                })
                .catch(function (error) {
                    console.log("error " + error);
                })
        }

        function search() {
            vm.showBrand = false;
            SearchService.search( {
                params: {
                    'brand': vm.brand
                }
            }).then(function (response) {
                vm.brand = response;
            }).catch(function (error) {
                console.log(error);
            })
        }

        function searchForBrand() {
            vm.showBrand = true;


            SearchService.search( {
                params: {
                    'brand': vm.brand
                }
            }).then(function (response) {
                vm.brand = response;
            }).catch(function (error) {
                console.log(error);
            })
        }

        /*vm.showDepartments = function (deptNo) {
            var departmentNo = deptNo;
            $state.go("B1", {deptNo: departmentNo})
        }*/
    }

})();