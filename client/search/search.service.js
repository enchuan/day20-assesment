(function () {
    angular
        .module("GroceryApp")
        .service("SearchService", SearchService);
    SearchService.$inject = ["$http", "$q"];
    function SearchService($http, $q) {
        var vm = this;

        vm.initProductData = function () {
            var defer = $q.defer();
            $http
                .get("/api/grocery_list")
                .then(function (response) {
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    console.log("error " + err);
                });
            return (defer.promise);
        };

        vm.search = function (ProductData) {
            var defer = $q.defer();
            $http
                .get("/api/grocery_list",
                    ProductData
                )
                .then(function (response) {
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    console.info("Error " + err);
                });
            return (defer.promise);
        };

        vm.searchForBrand = function (ProductData) {
            var defer = $q.defer();
            $http.get("/api/grocery_list",
                ProductData
                )
                .then(function (response) {
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    console.info("Error " + err);
                });
            return (defer.promise);
        }
    }
})();